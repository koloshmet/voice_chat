use tokio;
use tokio::io::{AsyncBufReadExt, AsyncReadExt, AsyncWriteExt};

async fn process_connection(socket: tokio::net::TcpStream) -> tokio::io::Result<()> {
    let mut buf = tokio::io::BufStream::new(socket);
    let mut line = String::new();
    buf.read_line(&mut line).await?;
    println!("{} Connected", line.trim());
    loop {
        let mut buff = [0; 1024];
        if buf.read(&mut buff).await? == 0 {
            return Ok(());
        }
        buf.write_all(&buff).await?;
    }
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = std::env::args().collect();

    let addr: std::net::SocketAddr = format!("0.0.0.0:{}", args[1]).parse().unwrap();
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();

    loop {
        let (socket, client_addr) = listener.accept().await.unwrap();
        tokio::spawn(async move {
            println!("Connection accepted from {}", client_addr);
            match process_connection(socket).await {
                Ok(_) => println!("Connection processed"),
                Err(e) => println!("Error: {}", e)
            }
        });
    }
}
