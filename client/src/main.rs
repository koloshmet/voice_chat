use cpal;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

use std::io::{Read, Write};

fn to_be_bytes(data: &[f32]) -> Vec<u8> {
    let mut res = Vec::<u8>::with_capacity(data.len() * 4);
    for float in data {
        for byte in float.to_be_bytes() {
            res.push(byte);
        }
    }
    return res;
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let addr: std::net::SocketAddr = args[1].parse().unwrap();
    let mut socket = std::net::TcpStream::connect(addr).unwrap();

    println!("Enter your name:");
    let mut name = String::new();
    std::io::stdin().read_line(&mut name).unwrap();
    socket.write_all(name.as_bytes()).unwrap();
    socket.flush().unwrap();

    let host = cpal::default_host();
    let in_device = host.default_input_device().unwrap();
    let in_config = in_device.default_input_config().unwrap().config();
    let out_device = host.default_output_device().unwrap();
    let out_config = out_device.default_output_config().unwrap().config();
    let err_fn = |err| eprintln!("An error occurred on the audio stream: {}", err);

    let mut write_buffer = std::io::BufWriter::new(socket.try_clone().unwrap());
    let in_stream =
        in_device.build_input_stream(
            &in_config,
            move |data: &[f32], _| {
                let bytes = to_be_bytes(data);
                write_buffer.write_all(&bytes).unwrap();
            },
            err_fn
        ).unwrap();

    let mut read_buffer = std::io::BufReader::new(socket.try_clone().unwrap());
    let out_stream =
        out_device.build_output_stream(
            &out_config,
            move |data: &mut [f32], _| {
                let mut resp = vec![0; data.len() * 4];
                let _rd = read_buffer.read(&mut resp).unwrap();
                for i in 0..data.len() {
                    let mut buff = [0u8; 4];
                    buff.copy_from_slice(&resp[i * 4 .. (i + 1) * 4]);
                    data[i] = f32::from_be_bytes(buff);
                }
            },
            err_fn
        ).unwrap();

    out_stream.play().unwrap();
    in_stream.pause().unwrap();
    let mut active = true;
    while active {
        let mut line = String::new();
        std::io::stdin().read_line(&mut line).unwrap();
        match &line.as_str()[..1] {
            "r" => {
                in_stream.play().unwrap()
            },
            "b" => {
                in_stream.pause().unwrap()
            },
            "e" => {
                active = false;
            }
            _ => {}
        }
    }
    socket.shutdown(std::net::Shutdown::Both).unwrap();
    println!("Exiting");
}
